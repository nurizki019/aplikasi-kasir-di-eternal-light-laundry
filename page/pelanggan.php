
<?php



include 'authcheck.php';

$view = $dbconnect->query('SELECT * FROM pelanggan');

?>

<!DOCTYPE html>
<html>
<head>
	<title>List Pelanggan</title>
	<link rel="stylesheet" href="/bootstrap/dist/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">

	<?php if (isset($_SESSION['success']) && $_SESSION['success'] != '') {?>

		<div class="alert alert-success" role="alert">
			<?=$_SESSION['success']?>
		</div>

	<?php
        }
        $_SESSION['success'] = '';
    ?>

	<h1>List pelanggan</h1>
	<a href="index.php?page=pelanggan_add" class="btn btn-primary">Tambah data</a>
	<hr>
	<table class="table table-bordered">
		<tr>
			<th>ID pelanggan</th>
			<th>Nama Pelanggan</th>
			<th>Alamat Pelanggan </th>
			<th>No Telepon</th>
			<th>ID Barang</th>
		</tr>
		<?php

        while ($row = $view->fetch_array()) { ?>

		<tr>
			<td> <?= $row['id_pelanggan'] ?> </td>
			<td> <?= $row['nama_pelanggan'] ?> </td>
			<td><?= $row['alamat_pelanggan'] ?></td>
			<td><?= $row['nomor_telepon']?></td>
			<td><?= $row['id_barang']?></td>
			<td>
				<a href="index.php?page=pelanggan_edit&id=<?= $row['id_pelanggan'] ?>">Edit</a> |
				<a href="page/pelanggan_delete.php?id=<?= $row['id_pelanggan'] ?>" onclick="return confirm('apakah anda yakin ingin menghapus ?')">Hapus</a>
			</td>
		</tr>

		<?php }
        ?>

	</table>
</div>
</body>
</html>
