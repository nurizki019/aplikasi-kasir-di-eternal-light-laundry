
<?php
include 'authcheck.php';

if (isset($_GET['id'])) {
    $id = $_GET['id'];

    //menampilkan data berdasarkan ID
    $data = mysqli_query($dbconnect, "SELECT * FROM pelanggan where id_pelanggan='$id'");
    $data = mysqli_fetch_assoc($data);
}

if (isset($_POST['update'])) {
    $id = $_GET['id'];

    $nama_pelanggan = $_POST['nama_pelanggan'];
    $alamat_pelanggan = $_POST['alamat_pelanggan'];
    $nomor_telepon = $_POST['nomor_telepon'];
    $id_barang = $_POST['id_barang'];
    // Menyimpan ke database;
    mysqli_query($dbconnect, "UPDATE pelanggan SET nama_pelanggan='$nama_pelanggan', alamat_pelanggan='$alamat_pelanggan', nomor_telepon='$nomor_telepon', id_barang='$id_barang' where id_pelanggan='$id' ");

    $_SESSION['success'] = 'Berhasil memperbaruhi data';

    // mengalihkan halaman ke list pelanggan
    header('location: index.php?page=pelanggan');
}

?>

<div class="container">
	<h1>Edit Pelanggan</h1>
	<form method="post">
	  <div class="form-group">
	    <label>Nama Pelanggan</label>
	    <input value="<?php echo $data["nama_pelanggan"]; ?>" type="text" name="nama_pelanggan" class="form-control" placeholder="Nama Pelanggan">
	  </div>
      <div class="form-group">
	    <label>Alamat Pelanggan</label>
	    <input value="<?php echo $data["alamat_pelanggan"]; ?>"  type="text" name="alamat_pelanggan" class="form-control" placeholder="Alamat Pelanggan">
	  </div>
      <div class="form-group">
	    <label>No Telepon</label>
	    <input value="<?php echo $data["nomor_telepon"]; ?>"  type="text" name="nomor_telepon" class="form-control" placeholder="Nomor Telepon">
	  </div>
	  <div class="form-group">
	    <label>ID Pelanggan</label>
	    <input value="<?php echo $data["id_barang"]; ?>"  type="text" name="id_barang" class="form-control" placeholder="Id Barang">
	  </div>
  		<input type="submit" name="update" value="Simpan" class="btn btn-primary">
  		<a href="?page=pelanggan" class="btn btn-warning">Kembali</a>
	</form>
</div>