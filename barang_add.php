
<?php

include 'config.php';
session_start();
include 'authcheck.php';

if (isset($_POST['simpan'])) {
	$nama = $_POST['nama'];
	$kode_barang = $_POST['kode_barang'];
	$harga = $_POST['harga'];
	// $jumlah = $_POST['jumlah'];


	// Menyimpan ke database;
	mysqli_query($dbconnect, "INSERT INTO `barang` VALUES (NULL,'$nama','$harga','$kode_barang')");

	$_SESSION['success'] = 'Berhasil menambahkan data';

	// mengalihkan halaman ke list barang
	header("location:barang.php");

}

?>

<!DOCTYPE html>
<html>
<head>
	<title>Tambah Laundry</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
</head>
<body>
<div class="container">
	<h1>Tambah Laundry</h1>
	<form method="POST">
	  <div class="form-group">
	    <label for="nama">Nama Barang</label>
	    <input type="text" name="nama" class="form-control" placeholder="Nama barang">
	  </div>
	  <div class="form-group">
	    <label for="kode_barang">Kode Barang</label>
	    <input type="text" name="kode_barang" class="form-control" placeholder="Kode barang">
	  </div>
	  <div class="form-group">
	    <label for="harga">Harga</label>
	    <input type="number" name="harga" class="form-control" placeholder="Harga Barang">
	  </div>
  	<input type="submit" name="simpan" value="simpan" class="btn btn-primary">
</form>
<a href="barang.php" class="btn btn-warning">Kembali</a>
</div>
</body>
</html>